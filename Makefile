install:
	cp teepkg /usr/bin/
	cp teepkg.1 /usr/share/man/man1/
	mkdir -p /usr/share/licenses/teepkg/
	cp LICENSE.txt /usr/share/licenses/teepkg/
bsd:
	cp teepkg /usr/local/bin/
	cp teepkg.1 /usr/local/share/man/man1/ 
	mkdir -p /usr/local/share/licenses/teepkg/
	cp LICENSE.txt /usr/local/share/licenses/teepkg/
uninstall:
	rm /usr/bin/teepkg
	rm /usr/local/bin/teepkg
	rm /usr/share/man/man1/teepkg.1
	rm -r /usr/share/licenses/teepkg
	rm -r /usr/local/share/licenses/teepkg
